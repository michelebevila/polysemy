from setuptools import setup
from setuptools.command.install import install as _install

class Install(_install): #https://stackoverflow.com/questions/26799894/installing-nltk-data-in-setup-py-script
    def run(self):
        _install.do_egg_install(self)
        import nltk
        nltk.download("wordnet")

setup(
    name='polysemy',
    version='0.1',
    description='A useful module',
    author='Michele Bevilacqua',
    author_email='michele.bevilacqua@uniroma1.it',
    packages=['polysemy'],  #same as name
    cmdclass={'install': Install},
    install_requires=['nltk'], #external packages as dependencies
    setup_requires=['nltk'],
    scripts=['bin/lemmas_from_pattern.py',]
)