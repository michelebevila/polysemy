if __name__ == "__main__":
    import sys
    sys.path.append("..")
    from argparse import ArgumentParser
    from polysemy.lemmas import get_all_lemmas, has_pattern
    from polysemy.lexnames import VALID_LEXNAMES

    DESCRIPTION = """Prints to stdout a list of tab-separated (lemma, pos) tuples from wordnet given a pattern of lexicographers' file.
    \n\nHere is a list of valid lexnames:
    """ + "\t" + "\n\t".join(VALID_LEXNAMES) + "\n"

    parser = ArgumentParser(description=DESCRIPTION)
    parser.add_argument('--pos', type=str, default="n", help='Must be in {n, v}')
    parser.add_argument('pattern', type=str, nargs='+', help="A pattern determined by any N of lexigraphers' file labels")

    args = parser.parse_args()

    lemma_pos = filter(lambda x:x[1] == args.pos, get_all_lemmas())
    hits = []
    for lemma, pos in lemma_pos:
        if has_pattern(lemma, pos, args.pattern):
            hits.append((lemma, pos,))

    print("\n".join(map("\t".join, hits)))