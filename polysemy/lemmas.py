from nltk.corpus import wordnet as wn

def get_all_lemmas():
    lemma_pos_set = set()
    for syn in wn.all_synsets():
        lemma_pos_set |= set([(l.name(), l.synset().pos()) for l in syn.lemmas()])
    return sorted(lemma_pos_set, key=lambda x:x[0])

def get_lexnames(lemma, pos):
    lexnames = [s.lexname() for s in wn.synsets(lemma, pos=pos)]
    return lexnames

def has_pattern(lemma, pos, pattern):
    lexnames = set(get_lexnames(lemma, pos))
    return not bool(set(pattern).difference(lexnames))

